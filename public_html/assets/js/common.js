function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';

    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
}

jQuery(document).ready(function(){
    let search_number = 0;
    let search_count = 0;
    let count_text = 0;

    $('#search_text').bind('keyup oncnange', function() {
        $('#text').removeHighlight();

        let search_text = $('#search_text').val();
        if (search_text == '') return;

        var str = search_text, mas = [], j = 0;
        for (i = 0; i < str.length; i++) {
            if (str[i] == " ") { j++; continue; }
            else {
                mas[j] ? mas[j] += str[i] : mas[j] = str[i];
            }
        }

        if (search_text[0] == '\'' && search_text[search_text.length - 1] == '\'') {
            search_text = search_text.slice(1, -1);
            $('#text').highlight(search_text, getRandomColor());

            search_count = $('#text span.highlight').size() - 1;
            count_text = search_count + 1;
            search_number = 0;
        } else {
            for (i = 0; i < mas.length; i++) {
                $('#text').highlight(mas[i], getRandomColor());

                search_count = $('#text span.highlight').size() - 1;
                count_text = search_count + 1;
                search_number = 0;
            }
        }

        $('#text').selectHighlight(search_number);
        $('#count').html('Count: <b>'+count_text+'</b>');
        $('#countValue').val(count_text);
    });

    $('#clear_button').click(function() {
        $('#text').removeHighlight();
        $('#count').html('');
        $('#countValue').val('');
        $('#search_text').val('');
    });


    $('#save_button').on('click', function () {
        $.ajax({
            method: 'POST',
            url: 'pages/ajax.php',
            data: {
                action: 'save',
                search_text: $('#search_text').val(),
                count: $('#countValue').val()
            }
        });
    });

    $('body').on('click', 'a', function (e) {
        e.preventDefault();
        let queryUrl = new URL($(this).attr('href'), location);
        let page = (queryUrl.search.split('=')[1] / 10) + 1;

        $.ajax({
            url: 'pages/ajax.php',
            type: 'POST',
            data: {
                action: 'getPagination',
                page: page,
            },
            success: function (msg) {
                msg = JSON.parse(msg);
                $('#pagination').parent('div').html(msg.pagination);
                $('#history').html(msg.history);
            }
        });
    });

    $('body').on('click', '#history > div', function () {
        $('#search_text').val($(this).html()).keyup();
    });
});