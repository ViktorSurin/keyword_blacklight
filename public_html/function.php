<?php

/**
 * Dump
 *
 * @param $var
 */
function dump($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

/**
 * Dump with die
 *
 * @param $var
 */
function dumpd($var)
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die;
}

/**
 * Connecting to database
 * 
 * @return mysqli
 */
function connectDB()
{
    $user = 'root';
    $host = 'localhost';
    $password = '12345';
    $database = 'keyword.loc';

    $mysqli = new mysqli($host, $user, $password, $database);

    if ($mysqli->connect_errno) {
        echo 'Не удалось подключиться к MySql: ('.$mysqli->connect_errno.') ' . $mysqli->connect_error;
    }

    return $mysqli;
}

/**
 * Get count rows from table
 *
 * @param mysqli $mysqli
 * @param string $table
 * @return int
 */
function getCount(mysqli $mysqli, string $table = '') : int
{
    if (!$mysqli) {
        die('Can\'t connect to DB. Error in file:' . __FILE__ .', function ' . __FUNCTION__);
    }

    if (!$table) {
        die('No table variable:' . __FILE__ .', function ' . __FUNCTION__);
    }

    $query = "SELECT COUNT(*) as count FROM $table";

    return mysqli_query($mysqli, $query)->fetch_assoc()['count'];
}

function getHistory(mysqli $mysqli, $offset = 0)
{
    $query = "SELECT * FROM `history` LIMIT $offset, 10";
    $results = mysqli_query($mysqli, $query);

    $history = '';
    while ($result = $results->fetch_assoc()) {
        $history .= '<div class="col-md-4">'.$result['value'].'</div>';
    }

    return $history;
}