<?php

/**
 * Class SimPageNav
 */
class SimPageNav
{
    protected $id;
    protected $startChar;
    protected $prevChar;
    protected $nextChar;
    protected $endChar;

    /**
     * Constructor
     * @param string $id        - attribute element ID <UL> - pagination
     * @param string $startChar - start link text
     * @param string $prevChar  - Back link text
     * @param string $nextChar  - forward link text
     * @param string $endChar   - link to end text
     */
    public function __construct( $id = 'pagination',
        $startChar = '&laquo;',
        $prevChar  = '&lsaquo;',
        $nextChar  = '&rsaquo;',
        $endChar   = '&raquo;'  )
    {
        $this->id = $id;
        $this->startChar = $startChar;
        $this->prevChar  = $prevChar;
        $this->nextChar  = $nextChar;
        $this->endChar   = $endChar;
    }

    /**
     * Get HTML paging code
     *
     * @param int $all        - Full number of elements (Materials in the category)
     * @param int $limit      - Number of elements per page
     * @param int $start      - Current item offset
     * @param int $linkLimit  - Number of links in state
     * @param string $varName - The name of GET is the variable that will be used in the build. navigation.
     * @return string
     */
    public function getLinks( /*int*/ $all, /*int*/ $limit, /*int*/ $start, $linkLimit = 10, $varName = 'start' )
    {
        // We don’t do anything if the limit is greater than or equal to the number of all elements in general,
        // And if the limit = 0. 0 - it will mean "do not split on the page."
        if ( $limit >= $all || $limit == 0 ) {
            return NULL;
        }

        $pages     = 0;       // number of pages in pagination
        $needChunk = 0;       // index of the currently needed chunk
        $queryVars = array(); // assoc. array obtained from the query string
        $pagesArr  = array(); // variable for intermediate storage of the navigation array
        $htmlOut   = '';      // HTML - page navigation code
        $link      = NULL;    // link forming

        // In this block, we simply build a link - the same as the one on which
        // came to this page, but we extract our GET variable from it:
        parse_str($_SERVER['QUERY_STRING'], $queryVars ); //   &$queryVars

        // Kill our get variable
        if( isset($queryVars[$varName]) ) {
            unset( $queryVars[$varName] );
        }

        // We form the same link leading to the same page:
        $link  = $_SERVER['PHP_SELF'].'?'.http_build_query( $queryVars );

        //--------------------------------------------------------

        $pages = ceil( $all / $limit ); // кол-во страниц

        // We fill the array: the key is the page number, the value is the offset for the database.
        // Numbering is needed here from one. A shift in steps = the number of materials on the page.
        for( $i = 0; $i < $pages; $i++) {
            $pagesArr[$i+1] = $i * $limit;
        }

        // Now, to display the desired number of links on the page
        // split the array with the values ​​[page number] => "offset" by
        // Parts (chunks)
        $allPages = array_chunk($pagesArr, $linkLimit, true);

        // We get the index of the chunk in which the desired offset is located.
        // And further only from it we will form the list of links:
        $needChunk = $this->searchPage( $allPages, $start );

        // We form links "To the beginning", "previous" ------------------------------------------------

        if ( $start > 1 ) {
            $htmlOut .= '<li><a href="'.$link.'&'.$varName.'=0">'.$this->startChar.'</a></li>'.
                '<li><a href="'.$link.'&'.$varName.'='.($start - $limit).'">'.$this->prevChar.'</a></li>';
        } else {
            $htmlOut .= '<li><span>'.$this->startChar.'</span></li>'.
                '<li><span>'.$this->prevChar.'</span></li>';
        }
        // Sobsno derive links from the desired chunk
        foreach( $allPages[$needChunk] AS $pageNum => $ofset )  {
            // We make the current page inactive:
            if( $ofset == $start  ) {
                $htmlOut .= '<li><span>'. $pageNum .'</span></li>';
                continue;
            }
            $htmlOut .= '<li><a href="'.$link.'&'.$varName.'='. $ofset .'">'. $pageNum . '</a></li>';
        }

        // We form the links "next", "to the end" ------------------------------------------------

        if ( ($all - $limit) >  $start) {
            $allPages = array_pop( $allPages );
            $allPages = array_pop($allPages);

            $htmlOut .= '<li><a href="' . $link . '&' . $varName . '=' . ( $start + $limit) . '">' . $this->nextChar . '</a></li>'.
                '<li><a href="' . $link . '&' . $varName . '=' . $allPages . '">' . $this->endChar . '</a></li>';
        } else {
            $htmlOut .= '<li><span>' . $this->nextChar . '</span></li>'.
                '<li><span>' . $this->endChar . '</span></li>';
        }
        return '<ul id="'.$this->id.'">' . $htmlOut . '<ul>';
    }


    /**
    * Looks for which chunk the page with offset $ needPage is in
    *
    * @param array $pagesList an array of chunks (an array of page pages)
    * @param int $needPage - offset
    *
    * @return number
    */
    protected function searchPage( array $pagesList, /*int*/$needPage )
    {
        foreach( $pagesList AS $chunk => $pages  ){
            if( in_array($needPage, $pages) ){
                return $chunk;
            }
        }
        return 0;
    }
}