<?php
require_once __DIR__.'/../function.php';
require_once __DIR__ .'/../lib/simPageNav.php';

$mysqli = connectDB();
$action = $_POST['action'];

if ($action == 'save') {
    $value = $_POST['search_text'];
    $count = $_POST['count'];
    $query = "INSERT INTO history (value, count) values('".$value."', '".$count."')";
    mysqli_query($mysqli, $query) or die('Ошибка' . mysqli_error($mysqli));
}

if ($action == 'getPagination') {
    $total = getCount($mysqli, 'history');
    $start = isset($_POST['page']) ? intval($_POST['page']) : 0;
    $offset= isset($_POST['page']) ? intval($_POST['page']) : 0;
    $limit = 10;
    $start = $limit * ($start -1);
    $pageNav = new SimPageNav();

    $result['pagination'] = $pageNav->getLinks($total, 10, $start, 5, 'start');
    $result['history'] = getHistory($mysqli, $offset);

    echo json_encode($result);
}